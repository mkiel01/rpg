#include "character.h"
#include "monster.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cmath>
using namespace std;

int main() {

    character your_hero;
    monster monsters[5]; // Create an array of monster objects

    while (true) {

        cout << "Choose your action" << endl;
        cout << "pick 1 to make new hero" << endl;
        cout << "pick 2 to select class of hero" << endl;
        cout << "pick 3 to show stats of hero" << endl;
        cout << "pick 4 to save current hero" << endl;
        cout << "pick 5 to load existing hero" << endl;
        cout << "pick 6 to generate 5 monsters" << endl;
        cout << "pick 7 to save monsters" << endl;
        cout << "pick 8 to end the game" << endl;

        int choice;
        cin >> choice;

        switch (choice) {

            case 1: {
                string tempname;
                double tempstrength;
                double tempdexterity;
                double tempendurance;
                double tempintelligence;
                double tempcharisma;


                cout << "Set name of character: ";
                cin >> tempname;
                cout << "Set strength of character: ";
                cin >> tempstrength;
                cout << "Set dexterity of character: ";
                cin >> tempdexterity;
                cout << "Set endurance of character: ";
                cin >> tempendurance;
                cout << "Set intelligence of character: ";
                cin >> tempintelligence;
                cout << "Set charisma of character: ";
                cin >> tempcharisma;

                your_hero = character(tempname, tempstrength, tempdexterity, tempendurance, tempintelligence,
                                                tempcharisma);

                break;
            }
            case 2:
                your_hero.setClass();

                break;

            case 3:
                your_hero.printStats();
                break;
            case 4:
                your_hero.save();
                break;
            case 5:
            {
                cout << "type the name of the hero you would like to load" << endl;
                string hero_name;
                cin >> hero_name;
                your_hero.load(hero_name);
                break;
            }

                break;
            case 6: {


                for (int i = 0; i < 5; i++) {
                    monsters[i] = monster();
                    cout << "Monster " << i << " with stats: " << endl;
                    monsters[i].printStats();
                    cout << "\n";

                }

                break;
            }
        case 7:
            monster::save(monsters);
        break;
        case 8:
            exit(0);

        default:
            cout << "pick a good option" << endl;

    }
    cout << "\nwish fufilled" << endl;
}


}






