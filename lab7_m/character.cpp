

#include "character.h"
#include <utility>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cmath>
using namespace std;

character::character(string newname, double newstrength, double newdexterity, double newendutance, double newintelligence, double newcharisma) {
    name = newname;
    strength = newstrength;
    dexterity = newdexterity;
    endurance = newendutance;
    intelligence = newintelligence;
    charisma = newcharisma;
}

void character::load(string filename) {

    fstream file;

    file.open(filename + ".txt", ios::in);
    name = filename;

    if(file.good()) {
        file>>name;
        file>>strength;
        file>>dexterity;
        file>>endurance;
        file>>intelligence;
        file>>charisma;
    }
    else {
        cout<<"Something went wrong with opening: "<<filename<<endl;
    }
    file.close();
}

void character::save() {
    fstream file;

    file.open(name +".txt", ios::out);

    if (file.good()) {
        file<<name<<endl;
        file<<strength<<endl;
        file<<dexterity<<endl;
        file<<endurance<<endl;
        file<<intelligence<<endl;
        file<<charisma<<endl;
    }

}

void character::printStats() {
    cout<<"Statistics of "<<name<<endl;
    cout<<"Strength = "<<strength<<endl;
    cout<<"Dexterity = "<<dexterity<<endl;
    cout<<"Endurance = "<<endurance<<endl;
    cout<<"Intelligence = "<<intelligence<<endl;
    cout<<"Charisma = "<<charisma<<endl;
}

