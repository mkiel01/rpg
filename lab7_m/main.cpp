#include "character.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cmath>
using namespace std;

int main() {

    string tempname;
    double tempstrength;
    double tempdexterity;
    double tempendurance;
    double tempintelligence;
    double tempcharisma;

    cout<<"Set name of character: ";
    cin>>tempname;
    cout<<"Set strength of character: ";
    cin>>tempstrength;
    cout<<"Set dexterity of character: ";
    cin>>tempdexterity;
    cout<<"Set endurance of character: ";
    cin>>tempendurance;
    cout<<"Set intelligence of character: ";
    cin>>tempintelligence;
    cout<<"Set charisma of character: ";
    cin>>tempcharisma;

    character michal = character(tempname, tempstrength, tempdexterity, tempendurance, tempintelligence, tempcharisma);
    michal.printStats();
    michal.save();
    michal.load("mich");
    michal.printStats();


    return 0;
}
