

#ifndef LAB7_M_CHARACTER_H
#define LAB7_M_CHARACTER_H
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <cmath>
using namespace std;


class character {
public:
    character(string,double,double,double,double, double);
    character(string);
    void save();
    void printStats();
    void create_character(string);
    void load(string);

private:

    string name;
    double strength;
    double dexterity;
    double endurance;
    double intelligence;
    double charisma;


};



#endif
